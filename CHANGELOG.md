## Changelog

- 0.1.0 working functionality, global seetings
- 0.2.0 add website settings
- 0.2.1 fix website settings in Profile detail
- 0.2.2 Remove warning in visitorDetail 
- 0.2.3 Remove warning in VisitLSSummary
- 1.0.0 Matomo 4.X version, allow to set url for LimeSurvey 4.X version
- 1.0.1 be sure to use integer in url