# Matomo LimeSurveyUserProfile Plugin

This plugin of for Matomo 4.X version.

## Usage

LimeSurvey user profile inside Matomo, need matomoPublicTrack plugin in LimeSurvey

Allow to save specific LimeSUrvey data inside matomo visit

- limesurvey_surveyid  : survey id
- limesurvey_responseid  : response id
- limesurvey_token : token used
- limesurvey_tokenid : token id
- limesurvey_tokeninfo : token information

## How to use

After activate the plugin, update Site settings in matomo SitesManager.

If settings is set and limesurvey information are provided by the LimeSurey plugin : you have in visitor profile :

- Survey adminitration link
- Token edition link
- Reponse admin view link
- Limesurvey version number
