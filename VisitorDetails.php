<?php
/**
 * LimeSurveyUserProfile plugin for matomo
 *
 * @link https://sondages.pro
 * @author Denis Chenu
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 * @since 0.1.0
 * @version 0.2.0
 */
namespace Piwik\Plugins\LimeSurveyUserProfile;

use Piwik\Piwik;
use Piwik\Plugins\Live\VisitorDetailsAbstract;
use Piwik\View;

class VisitorDetails extends VisitorDetailsAbstract
{
    protected $limesurvey = null;

    public function extendVisitorDetails(&$visitor)
    {
        $visitor['limesurvey'] = array(
            'surveyid' => $this->details['limesurvey_surveyid'],
            'responseid' => $this->details['limesurvey_responseid'],
            'token' => $this->details['limesurvey_token'],
            'tokenid' => $this->details['limesurvey_tokenid'],
            'tokeninfo' => $this->details['limesurvey_tokeninfo'],
            'responseinfo' => $this->details['user_id'],
        );
    }

    public function handleProfileVisit($visit, &$profile)
    {
        if (empty($this->limesurvey) && !empty($visit['limesurvey']['surveyid'])) {
            $this->limesurvey = $visit['limesurvey'];
        }
    }

    public function finalizeProfile($visits, &$profile)
    {
        $profile['limesurvey'] = $this->limesurvey;
    }

    public function renderVisitorDetails($visitInfo)
    {
        $view            = new View('@LimeSurveyUserProfile/_visitorDetails');
        $view->visitInfo = $visitInfo;
        $idSite = $visitInfo['idSite'];
        if (empty($visitInfo['limesurvey'])) {
            return [[ 40, '' ]];;
        }
        $limesurveyData = $visitInfo['limesurvey'];
        if (empty($limesurveyData['surveyid'])) {
            return [[ 40, '' ]];;
        }
        $view->limesurvey = $limesurveyData;
        $surveyid = intval($limesurveyData['surveyid']);
        $responseid = intval($limesurveyData['responseid']);
        $token = $limesurveyData['token'];
        $tokenid = intval($limesurveyData['tokenid']);

        $settings = new \Piwik\Plugins\LimeSurveyUserProfile\MeasurableSettings($idSite);
        if (empty($settings->adminurl->getValue())) {
            $settings = new \Piwik\Plugins\LimeSurveyUserProfile\SystemSettings();
        }
        $adminurl = $settings->adminurl->getValue();
        $urlFormat = $settings->urlFormat->getValue();
        $lsVersion = $settings->lsVersion->getValue();
        if ($adminurl) {
            $baseurl = rtrim($adminurl,"/") . "/";
            if ($settings->showScriptName->getValue()) {
                $baseurl .= "/index.php";
            }

            if ($urlFormat == "get") {
                $view->limesurvey['surveyLink'] = $baseurl."?r=admin/survey/sa/view&surveyid={$surveyid}";
            } else {
                $view->limesurvey['surveyLink'] = $baseurl."/admin/survey/sa/view/surveyid/{$surveyid}";
            }
            switch ($lsVersion) {
                case '3X':
                    switch ($urlFormat) {
                        case 'get':
                            $view->limesurvey['surveyLink'] = $baseurl."?r=admin/survey/sa/view&surveyid={$surveyid}";
                            if($responseid) {
                                $view->limesurvey['responseLink'] = $baseurl."?r=admin/responses/sa/view&surveyid={$surveyid}&id={$responseid}";
                            }
                            if($tokenid) {
                                $view->limesurvey['responseLink'] = $baseurl."?r=admin/tokens/sa/edit&iSurveyId={$surveyid}&iTokenId=$tokenid";
                            }
                        case 'path':
                        default:
                            $view->limesurvey['surveyLink'] = $baseurl."/admin/survey/sa/view/surveyid/{$surveyid}";
                            if($responseid) {
                                $view->limesurvey['responseLink'] = $baseurl."/admin/responses/sa/view?surveyid={$surveyid}&id=$responseid";
                            }
                            if($tokenid) {
                                $view->limesurvey['responseLink'] = $baseurl."/admin/tokens/sa/edit?iSurveyId={$surveyid}&iTokenId=$tokenid";
                            }
                    }
                    break;
                case '4X':
                default:
                    switch ($urlFormat) {
                        case 'get':
                            $view->limesurvey['surveyLink'] = $baseurl."?r=surveyAdministration/view&surveyid{$surveyid}";
                            if($responseid) {
                                $view->limesurvey['responseLink'] = $baseurl."?r=responses/view&surveyId=={$surveyid}&id={$responseid}";
                            }
                            if($tokenid) {
                                $view->limesurvey['responseLink'] = $baseurl."?r=admin/tokens/sa/edit&iSurveyId={$surveyid}&iTokenId=$tokenid";
                            }
                            break;
                        case 'path':
                        default:
                            $view->limesurvey['surveyLink'] = $baseurl."/surveyAdministration/view?surveyid={$surveyid}";
                            if($responseid) {
                                $view->limesurvey['responseLink'] = $baseurl."/responses/view?surveyId={$surveyid}&id=$responseid";
                            }
                            if($tokenid) {
                                $view->limesurvey['responseLink'] = $baseurl."/admin/tokens/sa/edit?iSurveyId={$surveyid}&iTokenId=$tokenid";
                            }
                    }
            }
        }
        if ($responseid) {
            $view->limesurvey['responsetitle'] = $responseid;
            if ($view->limesurvey['responseinfo']) {
                $view->limesurvey['responsetitle'] = $view->limesurvey['responseinfo'];
            }
        }
        if ($tokenid) {
            $view->limesurvey['tokentitle'] = $token;
            if ($view->limesurvey['tokeninfo']) {
                $view->limesurvey['tokentitle'] = $view->limesurvey['tokeninfo'];
            }
        }
        return [[ 40, $view->render() ]];
    }
}
