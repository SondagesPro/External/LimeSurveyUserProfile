<?php
/**
 * LimeSurveyUserProfile plugin for matomo
 *
 * @link https://sondages.pro
 * @author Denis Chenu
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 * @since 0.2.0
 */

namespace Piwik\Plugins\LimeSurveyUserProfile;
use Piwik\Measurable\Type\TypeManager;
use Piwik\Piwik;
use Piwik\Plugin;
use Piwik\Settings\Setting;
use Piwik\Settings\FieldConfig;
use Piwik\Validators\NotEmpty;
use Piwik\Plugins\SitesManager;
use Exception;

/**
 * Defines Settings for ExampleSettingsPlugin.
 *
 * Usage like this:
 * $settings = new MeasurableSettings($idSite);
 * $settings->autoRefresh->getValue();
 * $settings->metric->getValue();
 */
class MeasurableSettings extends \Piwik\Settings\Measurable\MeasurableSettings
{

    /** @var Setting */
    public $adminurl;

    /** @var Setting */
    public $urlFormat;

    /** @var Setting */
    public $showScriptName;

    /** @var Setting */
    public $lsVersion;

    protected function init()
    {
        // System setting --> The base url for admin part (@todo : replace by base url of site)
        $this->adminurl = $this->createAdminurlSetting();
        $this->urlFormat = $this->createUrlFormatSetting();
        $this->showScriptName = $this->createShowScriptNameSetting();
        $this->lsVersion = $this->createLsVersionSetting();
    }

    private function createAdminurlSetting()
    {
        return $this->makeSetting('adminurl', $default = '', FieldConfig::TYPE_STRING, function (FieldConfig $field) {
            $field->title = 'Url of limesurvey for link';
            $field->uiControl = FieldConfig::UI_CONTROL_URL;
            $field->description = 'Limesurvey : The base url for creation of administration link. Leave empty to use global default settings.';
            $field->validators[] = new NotEmpty();
        });
    }

    private function createUrlFormatSetting()
    {
        return $this->makeSetting('urlFormat', 'path', FieldConfig::TYPE_STRING, function (FieldConfig $field) {
            $field->title = 'Url format';
            $field->uiControl = FieldConfig::UI_CONTROL_SINGLE_SELECT;
            $field->availableValues = array('path' => 'path', 'get' => 'get');
            $field->description = 'Limesurvey : The url format : path if your url start by /, get if it start by ?r=';
        });
    }

    private function createShowScriptNameSetting()
    {
        return $this->makeSetting('showScriptName', true, FieldConfig::TYPE_BOOL, function (FieldConfig $field) {
            $field->title = 'Show Script Name';
            $field->uiControl = FieldConfig::UI_CONTROL_CHECKBOX;
            $field->description = 'Limesurvey : Adding index.php at start of url.';
        });
    }

    private function createLsVersionSetting()
    {
        return $this->makeSetting('lsVersion', '4X', FieldConfig::TYPE_STRING, function (FieldConfig $field) {
            $field->title = 'LimeSurvey version';
            $field->uiControl = FieldConfig::UI_CONTROL_SINGLE_SELECT;
            $field->availableValues = array('3X' => '3 or lesser', '4X' => '4 or upper');
            $field->description = 'The limesurvey version can be found at bottom right of your LimeSurey instance.';
        });
    }

}
