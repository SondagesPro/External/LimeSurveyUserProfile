<?php
/**
 * Piwik - free/libre analytics platform
 *
 * @link    http://piwik.org
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 * @version 1.0.2
 */

namespace Piwik\Plugins\LimeSurveyUserProfile\ProfileSummary;

use Piwik\Common;
use Piwik\Piwik;
use Piwik\Plugins\CustomVariables\Model;
use Piwik\Plugins\Live\ProfileSummary\ProfileSummaryAbstract;
use Piwik\View;

/**
 * Class VisitScopeSummary
 *
 * @api
 */
class VisitLSSummary extends ProfileSummaryAbstract
{
    public function getName()
    {
        return "LimeSurvey";
    }
    public function render()
    {
        $view              = new View('@LimeSurveyUserProfile/_profileSummary.twig');
        $view->visitorData = $this->profile;
        $view->limesurvey  = $this->profile['limesurvey'];
        if(empty($view->limesurvey['surveyid'])) {
            return;
        }
        $surveyid = intval($view->limesurvey['surveyid']);
        $responseid = intval($view->limesurvey['responseid']);
        $token = $view->limesurvey['token'];
        $tokenid = intval($view->limesurvey['tokenid']);
        $idSite      = Common::getRequestVar('idSite', null, 'int');
        $settings = new \Piwik\Plugins\LimeSurveyUserProfile\MeasurableSettings($idSite);
        if (empty($settings->adminurl->getValue())) {
            $settings = new \Piwik\Plugins\LimeSurveyUserProfile\SystemSettings();
        }
        $adminurl = $settings->adminurl->getValue();
        if($adminurl) {
            $baseurl = rtrim($adminurl,"/");
            $urlFormat = $settings->urlFormat->getValue();
            $lsVersion = $settings->lsVersion->getValue();
            if($settings->showScriptName->getValue()) {
                $baseurl .= "/index.php";
            }
            switch ($lsVersion) {
                case '3X':
                    switch ($urlFormat) {
                        case 'get':
                            $view->limesurvey['surveyLink'] = $baseurl."?r=admin/survey/sa/view&surveyid={$surveyid}";
                            if($responseid) {
                                $view->limesurvey['responseLink'] = $baseurl."?r=admin/responses/sa/view&surveyid={$surveyid}&id={$responseid}";
                            }
                            if($tokenid) {
                                $view->limesurvey['responseLink'] = $baseurl."?r=admin/tokens/sa/edit&iSurveyId={$surveyid}&iTokenId=$tokenid";
                            }
                            break;
                        case 'path':
                        default:
                            $view->limesurvey['surveyLink'] = $baseurl."/admin/survey/sa/view/surveyid/{$surveyid}";
                            if($responseid) {
                                $view->limesurvey['responseLink'] = $baseurl."/admin/responses/sa/view?surveyid={$surveyid}&id=$responseid";
                            }
                            if($tokenid) {
                                $view->limesurvey['responseLink'] = $baseurl."/admin/tokens/sa/edit?iSurveyId={$surveyid}&iTokenId=$tokenid";
                            }
                    }
                    break;
                case '4X':
                default:
                    switch ($urlFormat) {
                        case 'get':
                            $view->limesurvey['surveyLink'] = $baseurl."?r=r=surveyAdministration/view&surveyid{$surveyid}";
                            if($responseid) {
                                $view->limesurvey['responseLink'] = $baseurl."?r=responses/view&surveyId=={$surveyid}&id={$responseid}";
                            }
                            if($tokenid) {
                                $view->limesurvey['responseLink'] = $baseurl."?r=admin/tokens/sa/edit&iSurveyId={$surveyid}&iTokenId=$tokenid";
                            }
                            break;
                        case 'path':
                        default:
                            $view->limesurvey['surveyLink'] = $baseurl."/surveyAdministration/view?surveyid={$surveyid}";
                            if($responseid) {
                                $view->limesurvey['responseLink'] = $baseurl."/responses/view?surveyId={$surveyid}&id=$responseid";
                            }
                            if($tokenid) {
                                $view->limesurvey['responseLink'] = $baseurl."/admin/tokens/sa/edit?iSurveyId={$surveyid}&iTokenId=$tokenid";
                            }
                    }
            }
        }
        if($responseid) {
            $view->limesurvey['responsetitle'] = $responseid;
            if($view->limesurvey['responseinfo']) {
                $view->limesurvey['responsetitle'] = $view->limesurvey['responseinfo'];
            }
        }
        if($tokenid) {
            $view->limesurvey['tokentitle'] = $token;
            if($view->limesurvey['tokeninfo']) {
                $view->limesurvey['tokentitle'] = $view->limesurvey['tokeninfo'];
            }
        }
        return $view->render();
    }
    public function getOrder()
    {
        return 10;
    }
}
