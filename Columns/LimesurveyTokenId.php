<?php
/**
 * LimeSurveyUserProfile plugin for matomo
 *
 * @link https://sondages.pro
 * @author Denis Chenu
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 * @since 0.1.0
 *
 */
namespace Piwik\Plugins\LimeSurveyUserProfile\Columns;

use Piwik\Common;
use Piwik\Piwik;
use Piwik\Plugin\Dimension\VisitDimension;
use Piwik\Plugin\Segment;
use Piwik\Tracker\Request;
use Piwik\Tracker\Visitor;
use Piwik\Tracker\Action;

/**
 * @inheritDoc
 */
class LimesurveyTokenId extends VisitDimension
{
    /**
     * @inheritDoc
     */
    protected $nameSingular = 'LimeSurveyUserProfile_LimesurveyTokenId';

    /**
     * @inheritDoc
     */
    protected $columnName = 'limesurvey_tokenid';

    /**
     * @inheritDoc
     */
    protected $columnType = 'INTEGER(11) DEFAULT NULL';

    /**
     * @inheritDoc
     */
    protected $segmentName = 'tokenid';
    /**
     * @inheritDoc
     */
    protected $acceptValues = 'The token id if of the visit of current user.';

    /**
     * @inheritDoc
     */
    public function onNewVisit(Request $request, Visitor $visitor, $action)
    {
        $json = Common::getRequestVar('limesurveyData',[],'json');
        $tokenId = isset($json['tokenid']) ? intval($json['tokenid']) : null;
        if (empty($tokenId)) {
            return false;
        }
        return $tokenId;
    }

    /**
     * @inheritDoc
     */
    public function onExistingVisit(Request $request, Visitor $visitor, $action)
    {
        return $this->onNewVisit($request,  $visitor, $action);
    }

}
