<?php
/**
 * LimeSurveyUserProfile plugin for matomo
 *
 * @link https://sondages.pro
 * @author Denis Chenu
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 * @since 0.1.0
 *
 */
namespace Piwik\Plugins\LimeSurveyUserProfile\Columns;

use Piwik\Common;
use Piwik\Piwik;
use Piwik\Plugin\Dimension\VisitDimension;
use Piwik\Plugin\Segment;
use Piwik\Tracker\Request;
use Piwik\Tracker\Visitor;
use Piwik\Tracker\Action;

/**
 * @inheritDoc
 */
class LimesurveyToken extends VisitDimension
{
    /**
     * @inheritDoc
     */
    protected $nameSingular = 'LimeSurveyUserProfile_LimesurveyToken';

    /**
     * @inheritDoc
     */
    protected $columnName = 'limesurvey_token';

    /**
     * @inheritDoc
     */
    protected $columnType = 'VARCHAR(200) DEFAULT NULL';

    /**
     * @inheritDoc
     */
    protected $segmentName = 'token';

    /**
     * @inheritDoc
     */
    protected $acceptValues = 'The token of the visit of current user.';

    /**
     * @inheritDoc
     */
    public function onNewVisit(Request $request, Visitor $visitor, $action)
    {
        $json = Common::getRequestVar('limesurveyData',[],'json');
        $token = isset($json['token']) ? $json['token'] : null;
        if (empty($token)) {
            return false;
        }
        return $token;
    }

    /**
     * @inheritDoc
     */
    public function onExistingVisit(Request $request, Visitor $visitor, $action)
    {
        return $this->onNewVisit($request,  $visitor, $action);
    }

}
