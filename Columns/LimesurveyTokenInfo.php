<?php
/**
 * LimeSurveyUserProfile plugin for matomo
 *
 * @link https://sondages.pro
 * @author Denis Chenu
 * @license http://www.gnu.org/licenses/gpl-3.0.html GPL v3 or later
 * @since 0.1.0
 *
 */
namespace Piwik\Plugins\LimeSurveyUserProfile\Columns;

use Piwik\Common;
use Piwik\Piwik;
use Piwik\Plugin\Dimension\VisitDimension;
use Piwik\Plugin\Segment;
use Piwik\Tracker\Request;
use Piwik\Tracker\Visitor;
use Piwik\Tracker\Action;

/**
 * @inheritDoc
 */
class LimesurveyTokenInfo extends VisitDimension
{
    /**
     * @inheritDoc
     */
    protected $nameSingular = 'LimeSurveyUserProfile_LimesurveyTokenInfo';

    /**
     * @inheritDoc
     */
    protected $columnName = 'limesurvey_tokeninfo';

    /**
     * @inheritDoc
     */
    protected $columnType = 'TEXT DEFAULT NULL';

    /**
     * @inheritDoc
     */
    protected $segmentName = 'tokeninfo';
    /**
     * @inheritDoc
     */
    protected $acceptValues = 'The token information (lastname, firstnale and email) of the visit of current user.';

    /**
     * @inheritDoc
     */
    public function onNewVisit(Request $request, Visitor $visitor, $action)
    {
        $json = Common::getRequestVar('limesurveyData',[],'json');
        $tokeninfo= isset($json['tokeninfo']) ? $json['tokeninfo'] : null;
        if (empty($tokeninfo)) {
            return false;
        }
        return $tokeninfo;
    }

    /**
     * @inheritDoc
     */
    public function onExistingVisit(Request $request, Visitor $visitor, $action)
    {
        return $this->onNewVisit($request,  $visitor, $action);
    }

}
